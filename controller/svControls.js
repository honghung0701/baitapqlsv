function informationForm() {
    var maSv = document.getElementById('txtMaSV').value.trim();
    var tenSv = document.getElementById('txtTenSV').value.trim();
    var email = document.getElementById('txtEmail').value.trim();
    var matKhau = document.getElementById('txtPass').value.trim();
    var diemToan = document.getElementById('txtDiemToan').value.trim();
    var diemLy = document.getElementById('txtDiemLy').value.trim();
    var diemHoa = document.getElementById('txtDiemHoa').value.trim();
    var students = new Student (maSv,tenSv,email,matKhau,diemToan,diemLy,diemHoa);
    return students;
}
function renderStudentsList(list) {
    var contentHTML = "";
    for (var i = 0; i < list.length; i++) {
        var currentStudents = list[i];
        var contentTr = 
        `<Tr>
        <td>${currentStudents.ma}</td>
        <td>${currentStudents.ten}</td>
        <td>${currentStudents.email}</td>
        <td>${currentStudents.mediumScore()}</td>
        <td>
        <button onclick = "delStudent('${currentStudents.ma}')" class="btn btn-danger">Xóa</button>
        <button onclick = "fixStudent('${currentStudents.ma}')" class="btn btn-primary">Sửa</button>
        </td>
        </Tr>`
        contentHTML= contentHTML + contentTr;
    }
    document.getElementById('tbodySinhVien').innerHTML = contentHTML;
}
function showInformationForm(sinhVien) {
  document.getElementById("txtMaSV").value = sinhVien.ma;
  document.getElementById("txtTenSV").value = sinhVien.ten;
  document.getElementById("txtEmail").value = sinhVien.email;
  document.getElementById("txtPass").value = sinhVien.matKhau;
  document.getElementById("txtDiemLy").value = sinhVien.diemLy;
  document.getElementById("txtDiemHoa").value = sinhVien.diemHoa;
  document.getElementById("txtDiemToan").value = sinhVien.diemToan;
}

function resetForm() {
    document.getElementById('formQLSV').reset();
}