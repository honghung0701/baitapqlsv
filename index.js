var studentsList = [];
// const DSSV = "DSSV";
// var dataJson = localStorage.getItem("DSSV");
// if (dataJson) {
//     var dataRaw = JSON.parse(dataJson)
//     // studentsList = dataRaw.map(function (item) {
//     //     return new Student(
//     //         item.ma,
//     //         item.ten,
//     //         item.email,
//     //         item.matKhau,
//     //         item.diemToan,
//     //         item.diemHoa,
//     //         item.diemLy,
//     //     )
//     // });
//     renderStudentsList();
// }
var dataJson = localStorage.getItem("DSSV");
if (dataJson) {
    var dataRaw = JSON.parse(dataJson);
    var result = [];
    for (var index = 0; index < dataRaw.length; index ++ ) {
        var currentData = dataRaw[index];
        var sv = new Student (
            currentData.ma, 
            currentData.ten, 
            currentData.email, 
            currentData.matKhau, 
            currentData.diemToan,
            currentData.diemLy,
            currentData.diemHoa
        );
        result.push(sv);
    }
    studentsList = result;
    renderStudentsList(studentsList);
}
function saveLocalStorage() {
    var dssvJson = JSON.stringify(studentsList);
    localStorage.setItem("DSSV",dssvJson);
}

function moreStudents() {
    var newSv = informationForm();
    var isValid = true;
    isValid &= checkEmpty(newSv.ma,"spanMaSV") && checkMaSv(newSv.ma,studentsList,"spanMaSV");
    isValid &= checkEmpty(newSv.ten,"spanTenSV");
    isValid &= checkEmpty(newSv.email,"spanEmailSV") && checkEmail(newSv.email,"spanEmailSV");
    isValid &= checkEmpty(newSv.matKhau,"spanMatKhau");
    isValid &= checkEmpty(newSv.diemToan,"spanToan");
    isValid &= checkEmpty(newSv.diemLy,"spanLy");
    isValid &= checkEmpty(newSv.diemHoa,"spanHoa");
    if (isValid) {
        studentsList.push(newSv);
        saveLocalStorage();
        renderStudentsList(studentsList);
        resetForm();
    }
}
function delStudent(idSv) {
    var index = studentsList.findIndex(function (sv) {
        return sv.ma == idSv;
    })
    if (index == -1) {
        return;
    }
    studentsList.splice(index,1);
    saveLocalStorage(studentsList);
    renderStudentsList(studentsList);
}
function fixStudent(idSv) {
    var index = studentsList.findIndex(function (sv) {
        return sv.ma == idSv;
    });
    if (index == -1) {
        return;
    };
    var sv = studentsList[index];
    showInformationForm(sv);
    document.getElementById("txtMaSV").disabled = true;
}
function updateStudent() {
    var svEdit = informationForm();
    var index = studentsList.findIndex(function (sv) {
        return sv.ma == svEdit.ma;
    });
    if (index == -1) return;
    studentsList[index] = svEdit;
    renderStudentsList(studentsList);
    resetForm();
    document.getElementById("txtMaSV").style.disabled = false;
} 
function resetAtForm() {
    resetForm();
}
// SEARCH
function searchUser() {
    var valueSearch = document.getElementById('txtSearch').value;
    var userSearch = studentsList.filter(value => {
        return value.ten.toUpperCase().includes(valueSearch.toUpperCase())
    });
    renderStudentsList(userSearch);
}